<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Golonganmaster extends CI_Controller {
    public $title = "Golongan Audit";
    public $tbl_main = "ms_gol_audit";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "golongan";
        $data["title"] = $this->title;
		$data["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_gol_audit"=>"0"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_ms_gol_audit"])){
        	$id_ms_gol_audit = $this->input->post('id_ms_gol_audit');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_ms_gol_audit"=>$id_ms_gol_audit));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'nama_ms_gol_audit',
                'label'=>'nama_ms_gol_audit',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_ms_gol_audit"=>""
                );

        if($this->val_form_insert()){
            
            $nama_ms_gol_audit 	= $this->input->post("nama_ms_gol_audit", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nama_ms_gol_audit],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data($this->tbl_main, ["nama_ms_gol_audit"], [$nama_ms_gol_audit]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nama Sudah Terdaftar pada Sistem");
                }else{
                    $data = [
                        "id_ms_gol_audit"=>0,
                        "nama_ms_gol_audit"=>$nama_ms_gol_audit,
                        "sts_ms_gol_audit"=>"1"
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_ms_gol_audit"]= strip_tags(form_error('nama_ms_gol_audit'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_ms_gol_audit"=>"",
                );

        if($this->val_form_insert()){
            $id_ms_gol_audit 		= $this->input->post("id_ms_gol_audit", true);

            $nama_ms_gol_audit 	= $this->input->post("nama_ms_gol_audit", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nama_ms_gol_audit],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data_up($this->tbl_main, ["id_ms_gol_audit","nama_ms_gol_audit"], [$id_ms_gol_audit,$nama_ms_gol_audit]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nama Sudah Terdaftar pada Sistem");
                }else{
                    $set = [
                        "nama_ms_gol_audit"=>$nama_ms_gol_audit,
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $where = array("id_ms_gol_audit"=>$id_ms_gol_audit);
                    $update = $this->mm->update_data($this->tbl_main, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_ms_gol_audit"]= strip_tags(form_error('nama_ms_gol_audit'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

public function delete_data(){
    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
    $msg_detail = array(
                "id_ms_gol_audit"=>"",
            );

    if($_POST["id_ms_gol_audit"]){
        $id_ms_gol_audit = $this->input->post("id_ms_gol_audit");
        $where = array("id_ms_gol_audit"=>$id_ms_gol_audit);

        $set = array("is_del_ms_gol_audit"=>"1");

        // $delete_admin = $this->mm->delete_data($this->tbl_main, $where);
        $delete_admin = $this->mm->update_data($this->tbl_main, $set, $where);
        
        if($delete_admin){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
    }else{
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail["id_ms_gol_audit"]= strip_tags(form_error('id_ms_gol_audit'));        
    }

    $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_gol_audit"=>"0"));
    $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
    print_r(json_encode($res_msg));
}
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================
    public function check_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_ms_gol_audit"=>"",
                );

        if($_POST["id_ms_gol_audit"]){
            $id_ms_gol_audit = $this->input->post("id_ms_gol_audit");
            $param = $this->input->post("param");

            $where 	= array("id_ms_gol_audit"=>$id_ms_gol_audit);
            if ($param == "non_active") {
                $set 	= array("sts_ms_gol_audit"=>"0");
            }else{
                $set 	= array("sts_ms_gol_audit"=>"1");
            }
            
            
            $update_data = $this->mm->update_data($this->tbl_main, $set, $where);
            if($update_data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_ms_gol_audit"]= strip_tags(form_error('id_ms_gol_audit'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_gol_audit"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
}
