<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skpdmaster extends CI_Controller {
    public $title = "SKPD";
    public $tbl_main = "ms_skpd";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "skpd";
        $data["title"] = $this->title;
		$data["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_skpd"=>"0"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_ms_skpd"])){
        	$id_ms_skpd = $this->input->post('id_ms_skpd');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_ms_skpd"=>$id_ms_skpd));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'alamat_ms_skpd',
                'label'=>'alamat_ms_skpd',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'kd_ms_skpd',
                'label'=>'kd_ms_skpd',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'nama_ms_skpd',
                'label'=>'nama_ms_skpd',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'singkatan_ms_skpd',
                'label'=>'singkatan_ms_skpd',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "alamat_ms_skpd"=>"",
                    "kd_ms_skpd"=>"",
                    "nama_ms_skpd"=>"",
                    "singkatan_ms_skpd"=>"",
                );

        if($this->val_form_insert()){
            
            $alamat_ms_skpd 	= $this->input->post("alamat_ms_skpd", true);
            $kd_ms_skpd 			= $this->input->post("kd_ms_skpd", true);
            $nama_ms_skpd 		= $this->input->post("nama_ms_skpd", true);
            $singkatan_ms_skpd 	= $this->input->post("singkatan_ms_skpd", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $alamat_ms_skpd],
                                [$type_pattern, $kd_ms_skpd],
                                [$type_pattern, $nama_ms_skpd],
                                [$type_pattern, $singkatan_ms_skpd],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data($this->tbl_main, ["nama_ms_skpd","kd_ms_skpd"], [$nama_ms_skpd,$kd_ms_skpd]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nama / Kode Sudah Terdaftar pada Sistem");
                }else{
                    $data = [
                        "id_ms_skpd"=>"",
                        "alamat_ms_skpd"=>$alamat_ms_skpd,
                        "kd_ms_skpd"=>$kd_ms_skpd,
                        "nama_ms_skpd"=>$nama_ms_skpd,
                        "singkatan_ms_skpd"=>$singkatan_ms_skpd,
                        "sts_ac_ms_skpd"=>"1"
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["alamat_ms_skpd"]= strip_tags(form_error('alamat_ms_skpd'));
            $msg_detail["kd_ms_skpd"] 		= strip_tags(form_error('kd_ms_skpd'));
            $msg_detail["nama_ms_skpd"] 	= strip_tags(form_error('nama_ms_skpd'));
            $msg_detail["singkatan_ms_skpd"] 	= strip_tags(form_error('singkatan_ms_skpd'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "alamat_ms_skpd"=>"",
                    "kd_ms_skpd"=>"",
                    "nama_ms_skpd"=>"",
                    "singkatan_ms_skpd"=>"",
                );

        if($this->val_form_insert()){
            $id_ms_skpd 		= $this->input->post("id_ms_skpd", true);

            $alamat_ms_skpd 	= $this->input->post("alamat_ms_skpd", true);
            $kd_ms_skpd 			= $this->input->post("kd_ms_skpd", true);
            $nama_ms_skpd 		= $this->input->post("nama_ms_skpd", true);
            $singkatan_ms_skpd 	= $this->input->post("singkatan_ms_skpd", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $alamat_ms_skpd],
                                [$type_pattern, $kd_ms_skpd],
                                [$type_pattern, $nama_ms_skpd],
                                [$type_pattern, $singkatan_ms_skpd],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data_up($this->tbl_main, ["id_ms_skpd","nama_ms_skpd","kd_ms_skpd"], [$id_ms_skpd,$nama_ms_skpd,$kd_ms_skpd]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nama / Kode Sudah Terdaftar pada Sistem");
                }else{
                    $set = [
                        "alamat_ms_skpd"=>$alamat_ms_skpd,
                        "kd_ms_skpd"=>$kd_ms_skpd,
                        "nama_ms_skpd"=>$nama_ms_skpd,
                        "singkatan_ms_skpd"=>$singkatan_ms_skpd
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $where = array("id_ms_skpd"=>$id_ms_skpd);
                    $update = $this->mm->update_data($this->tbl_main, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["alamat_ms_skpd"]= strip_tags(form_error('alamat_ms_skpd'));
            $msg_detail["kd_ms_skpd"] 		= strip_tags(form_error('kd_ms_skpd'));
            $msg_detail["nama_ms_skpd"] 	= strip_tags(form_error('nama_ms_skpd'));
            $msg_detail["singkatan_ms_skpd"] 	= strip_tags(form_error('singkatan_ms_skpd'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

public function delete_data(){
    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
    $msg_detail = array(
                "id_ms_skpd"=>"",
            );

    if($_POST["id_ms_skpd"]){
        $id_ms_skpd = $this->input->post("id_ms_skpd");
        $where = array("id_ms_skpd"=>$id_ms_skpd);

        $set = array("is_del_ms_skpd"=>"1");

        // $delete_admin = $this->mm->delete_data($this->tbl_main, $where);
        $delete_admin = $this->mm->update_data($this->tbl_main, $set, $where);
        
        if($delete_admin){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
    }else{
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail["id_ms_skpd"]= strip_tags(form_error('id_ms_skpd'));        
    }

    $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_skpd"=>"0"));
    $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
    print_r(json_encode($res_msg));
}
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================
    public function check_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_ms_skpd"=>"",
                );

        if($_POST["id_ms_skpd"]){
            $id_ms_skpd = $this->input->post("id_ms_skpd");
            $param = $this->input->post("param");

            $where 	= array("id_ms_skpd"=>$id_ms_skpd);
            if ($param == "non_active") {
                $set 	= array("sts_ac_ms_skpd"=>"0");
            }else{
                $set 	= array("sts_ac_ms_skpd"=>"1");
            }
            
            
            $update_data = $this->mm->update_data($this->tbl_main, $set, $where);
            if($update_data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_ms_skpd"]= strip_tags(form_error('id_ms_skpd'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_skpd"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
}
