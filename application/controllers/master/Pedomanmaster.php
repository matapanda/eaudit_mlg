<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedomanmaster extends CI_Controller {
    public $title = "Pedoman";
    public $tbl_main = "ms_ped_audit";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "pedoman";
        $data["title"] = $this->title;
        $data["list_data"] = $this->db->query("SELECT a.*,b.nama_ms_jn_audit AS nm_jenis, c.nama_ms_ssrn_audit AS nm_sasaran FROM ms_ped_audit a LEFT JOIN ms_jn_audit b ON a.ms_jn_audit_id = b.id_ms_jn_audit LEFT JOIN ms_ssrn_audit c ON a.ms_ssrn_audit_id = c.id_ms_ssrn_audit where a.is_del_ms_ped_audit = '0' ")->result();
        $data["select_jn"] = $this->mm->get_data_all_where("ms_jn_audit", array("sts_ms_jn_audit"=>"1", "is_del_ms_jn_audit"=> "0"));
        $data["select_ssrn"] = $this->mm->get_data_all_where("ms_ssrn_audit", array("sts_ms_ssrn_audit"=>"1", "is_del_ms_ssrn_audit"=> "0"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_ms_ped_audit"])){
        	$id_ms_ped_audit = $this->input->post('id_ms_ped_audit');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_ms_ped_audit"=>$id_ms_ped_audit));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'kd_ped_audit',
                'label'=>'kd_ped_audit',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'ms_jn_audit_id',
                'label'=>'ms_jn_audit_id',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ms_ssrn_audit_id',
                'label'=>'ms_ssrn_audit_id',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_ped_audit"=>"",
                    "ms_jn_audit_id"=>"",
                    "ms_ssrn_audit_id"=>"",
                );

        if($this->val_form_insert()){
            $ms_ssrn_audit_id 	= $this->input->post("ms_ssrn_audit_id", true);
            $kd_ped_audit 			= $this->input->post("kd_ped_audit", true);
            $ms_jn_audit_id 	= $this->input->post("ms_jn_audit_id", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [
                                [$type_pattern, $kd_ped_audit],
                                [$type_pattern, $ms_jn_audit_id],
                                [$type_pattern, $ms_ssrn_audit_id],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data($this->tbl_main, ["kd_ped_audit"], [$kd_ped_audit]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Kode Sudah Terdaftar pada Sistem");
                }else{
                    $data = [
                        "id_ms_ped_audit"=>"",
                        "kd_ped_audit"=>$kd_ped_audit,
                        "ms_jn_audit_id"=>$ms_jn_audit_id,
                        "ms_ssrn_audit_id"=>$ms_ssrn_audit_id,
                        "sts_ac_ms_ped_audit"=>"1"
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_ped_audit"] 		= strip_tags(form_error('kd_ped_audit'));
            $msg_detail["ms_jn_audit_id"] 	= strip_tags(form_error('ms_jn_audit_id')); 
            $msg_detail["ms_ssrn_audit_id"] 	= strip_tags(form_error('ms_ssrn_audit_id'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_ped_audit"=>"",
                    "ms_jn_audit_id"=>"",
                    "ms_ssrn_audit_id"=>"",
                );

        if($this->val_form_insert()){
            $id_ms_ped_audit 		= $this->input->post("id_ms_ped_audit", true);

            $ms_ssrn_audit_id 	= $this->input->post("ms_ssrn_audit_id", true);
            $kd_ped_audit 			= $this->input->post("kd_ped_audit", true);
            $ms_jn_audit_id 	= $this->input->post("ms_jn_audit_id", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [
                                [$type_pattern, $kd_ped_audit],
                                [$type_pattern, $ms_jn_audit_id],
                                [$type_pattern, $ms_ssrn_audit_id],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data_up($this->tbl_main, ["id_ms_ped_audit","kd_ped_audit"], [$id_ms_ped_audit,$kd_ped_audit]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Kode Sudah Terdaftar pada Sistem");
                }else{
                    $set = [
                        "kd_ped_audit"=>$kd_ped_audit,
                        "ms_jn_audit_id"=>$ms_jn_audit_id,
                        "ms_ssrn_audit_id"=>$ms_ssrn_audit_id
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $where = array("id_ms_ped_audit"=>$id_ms_ped_audit);
                    $update = $this->mm->update_data($this->tbl_main, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_ped_audit"] 		= strip_tags(form_error('kd_ped_audit'));
            $msg_detail["ms_jn_audit_id"] 	= strip_tags(form_error('ms_jn_audit_id')); 
            $msg_detail["ms_ssrn_audit_id"] 	= strip_tags(form_error('ms_ssrn_audit_id'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

public function delete_data(){
    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
    $msg_detail = array(
                "id_ms_ped_audit"=>"",
            );

    if($_POST["id_ms_ped_audit"]){
        $id_ms_ped_audit = $this->input->post("id_ms_ped_audit");
        $where = array("id_ms_ped_audit"=>$id_ms_ped_audit);

        $set = array("is_del_ms_ped_audit"=>"1");

        // $delete_admin = $this->mm->delete_data($this->tbl_main, $where);
        $delete_admin = $this->mm->update_data($this->tbl_main, $set, $where);
        
        if($delete_admin){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
    }else{
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail["id_ms_ped_audit"]= strip_tags(form_error('id_ms_ped_audit'));        
    }

    $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("id_ms_ped_audit !="=>"0", "is_del_ms_ped_audit"=>"0"));
    $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
    print_r(json_encode($res_msg));
}
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================
    public function check_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_ms_ped_audit"=>"",
                );

        if($_POST["id_ms_ped_audit"]){
            $id_ms_ped_audit = $this->input->post("id_ms_ped_audit");
            $param = $this->input->post("param");

            $where 	= array("id_ms_ped_audit"=>$id_ms_ped_audit);
            if ($param == "non_active") {
                $set 	= array("sts_ac_ms_ped_audit"=>"0");
            }else{
                $set 	= array("sts_ac_ms_ped_audit"=>"1");
            }
            
            
            $update_data = $this->mm->update_data($this->tbl_main, $set, $where);
            if($update_data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_ms_ped_audit"]= strip_tags(form_error('id_ms_ped_audit'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("id_ms_ped_audit !="=>"0", "is_del_ms_ped_audit"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
}
