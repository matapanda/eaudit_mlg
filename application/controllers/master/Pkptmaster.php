<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkptmaster extends CI_Controller {
    public $title = "PKPT";
    public $tbl_main = "ms_pkpt";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "pkpt";
        $data["title"] = $this->title;
		$data["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_pkpt"=>"0"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_ms_pkpt"])){
        	$id_ms_pkpt = $this->input->post('id_ms_pkpt');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_ms_pkpt"=>$id_ms_pkpt));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'no_pkpt',
                'label'=>'no_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'thn_pkpt',
                'label'=>'thn_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'nama_pkpt',
                'label'=>'nama_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'tema_pkpt',
                'label'=>'tema_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'kgtn_pkpt',
                'label'=>'kgtn_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'hp_pkpt',
                'label'=>'hp_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'rmp_pkpt',
                'label'=>'rmp_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'rpl_pkpt',
                'label'=>'rpl_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'dana_pkpt',
                'label'=>'dana_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'resiko_pkpt',
                'label'=>'resiko_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'ket_pkpt',
                'label'=>'ket_pkpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "no_pkpt"=>"",
                    "thn_pkpt"=>"",
                    "nama_pkpt"=>"",
                    "tema_pkpt"=>"",
                    "kgtn_pkpt"=>"",
                    "hp_pkpt"=>"",
                    "rmp_pkpt"=>"",
                    "rpl_pkpt"=>"",
                    "dana_pkpt"=>"",
                    "resiko_pkpt"=>"",
                    "ket_pkpt"=>"",
                );

        if($this->val_form_insert()){
            
            $no_pkpt 		    = $this->input->post("no_pkpt", true);
            $thn_pkpt 		    = $this->input->post("thn_pkpt", true);
            $nomer              = $thn_pkpt.$no_pkpt;
            $nama_pkpt 		    = $this->input->post("nama_pkpt", true);
            $tema_pkpt 		    = $this->input->post("tema_pkpt", true);
            $kgtn_pkpt 		    = $this->input->post("kgtn_pkpt", true);
            $hp_pkpt 		    = $this->input->post("hp_pkpt", true);
            $rmp_pkpt 		    = $this->input->post("rmp_pkpt", true);
            $rpl_pkpt 		    = $this->input->post("rpl_pkpt", true);
            $dana_pkpt 		    = $this->input->post("dana_pkpt", true);
            $resiko_pkpt 		= $this->input->post("resiko_pkpt", true);
            $ket_pkpt 		    = $this->input->post("ket_pkpt", true);

            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [
                                [$type_pattern, $no_pkpt],
                                [$type_pattern, $thn_pkpt],
                                [$type_pattern, $nama_pkpt],
                                [$type_pattern, $tema_pkpt],
                                [$type_pattern, $kgtn_pkpt],
                                [$type_pattern, $hp_pkpt],
                                [$type_pattern, $rmp_pkpt],
                                [$type_pattern, $rpl_pkpt],
                                [$type_pattern, $dana_pkpt],
                                [$type_pattern, $resiko_pkpt],
                                [$type_pattern, $ket_pkpt],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data($this->tbl_main, ["no_pkpt"], [$nomer]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nomer PKPT Sudah Terdaftar pada Sistem");
                }else{
                    $data = [
                        "id_ms_pkpt"=>0,
                        "no_pkpt"=>$nomer,
                        "thn_pkpt"=>$thn_pkpt,
                        "nama_pkpt"=>$nama_pkpt,
                        "tema_pkpt"=>$tema_pkpt,
                        "kgtn_pkpt"=>$kgtn_pkpt,
                        "hp_pkpt"=>$hp_pkpt,
                        "rmp_pkpt"=>$rmp_pkpt,
                        "rpl_pkpt"=>$rpl_pkpt,
                        "dana_pkpt"=>$dana_pkpt,
                        "resiko_pkpt"=>$resiko_pkpt,
                        "ket_pkpt"=>$ket_pkpt,
                        "sts_ac_ms_pkpt"=>"1"
                    ];
                    //test
                    // $msg_main = array("status"=>$nomer, "msg"=>$_POST);
                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["no_pkpt"]  	= strip_tags(form_error('no_pkpt'));
            $msg_detail["thn_pkpt"] 	= strip_tags(form_error('thn_pkpt'));
            $msg_detail["nama_pkpt"] 	= strip_tags(form_error('nama_pkpt'));
            $msg_detail["tema_pkpt"] 	= strip_tags(form_error('tema_pkpt'));
            $msg_detail["kgtn_pkpt"] 	= strip_tags(form_error('kgtn_pkpt'));
            $msg_detail["hp_pkpt"] 	    = strip_tags(form_error('hp_pkpt'));
            $msg_detail["rmp_pkpt"] 	= strip_tags(form_error('rmp_pkpt'));
            $msg_detail["rpl_pkpt"] 	= strip_tags(form_error('rpl_pkpt'));
            $msg_detail["dana_pkpt"] 	= strip_tags(form_error('dana_pkpt'));
            $msg_detail["resiko_pkpt"] 	= strip_tags(form_error('resiko_pkpt'));
            $msg_detail["ket_pkpt"] 	= strip_tags(form_error('ket_pkpt'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "no_pkpt"=>"",
                    "thn_pkpt"=>"",
                    "nama_pkpt"=>"",
                    "tema_pkpt"=>"",
                    "kgtn_pkpt"=>"",
                    "hp_pkpt"=>"",
                    "rmp_pkpt"=>"",
                    "rpl_pkpt"=>"",
                    "dana_pkpt"=>"",
                    "resiko_pkpt"=>"",
                    "ket_pkpt"=>"",
                );

        if($this->val_form_insert()){
            $id_ms_pkpt 	= $this->input->post("id_ms_pkpt", true);
            $no_pkpt 		= $this->input->post("no_pkpt", true);
            $thn_pkpt 		= $this->input->post("thn_pkpt", true);
            $nomer          = $thn_pkpt.$no_pkpt;
            $nama_pkpt 		= $this->input->post("nama_pkpt", true);
            $tema_pkpt 		= $this->input->post("tema_pkpt", true);
            $kgtn_pkpt 		= $this->input->post("kgtn_pkpt", true);
            $hp_pkpt 		= $this->input->post("hp_pkpt", true);
            $rmp_pkpt 		= $this->input->post("rmp_pkpt", true);
            $rpl_pkpt 		= $this->input->post("rpl_pkpt", true);
            $dana_pkpt 		= $this->input->post("dana_pkpt", true);
            $resiko_pkpt 	= $this->input->post("resiko_pkpt", true);
            $ket_pkpt 		= $this->input->post("ket_pkpt", true);

            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [
                                [$type_pattern, $no_pkpt],
                                [$type_pattern, $thn_pkpt],
                                [$type_pattern, $nama_pkpt],
                                [$type_pattern, $tema_pkpt],
                                [$type_pattern, $kgtn_pkpt],
                                [$type_pattern, $hp_pkpt],
                                [$type_pattern, $rmp_pkpt],
                                [$type_pattern, $rpl_pkpt],
                                [$type_pattern, $dana_pkpt],
                                [$type_pattern, $resiko_pkpt],
                                [$type_pattern, $ket_pkpt],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data_up($this->tbl_main, ["id_ms_pkpt","no_pkpt"], [$id_ms_pkpt,$nomer]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nomer PKPT Sudah Terdaftar pada Sistem");
                }else{
                    $set = [
                        "no_pkpt"=>$nomer,
                        "thn_pkpt"=>$thn_pkpt,
                        "nama_pkpt"=>$nama_pkpt,
                        "tema_pkpt"=>$tema_pkpt,
                        "kgtn_pkpt"=>$kgtn_pkpt,
                        "hp_pkpt"=>$hp_pkpt,
                        "rmp_pkpt"=>$rmp_pkpt,
                        "rpl_pkpt"=>$rpl_pkpt,
                        "dana_pkpt"=>$dana_pkpt,
                        "resiko_pkpt"=>$resiko_pkpt,
                        "ket_pkpt"=>$ket_pkpt,
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $where = array("id_ms_pkpt"=>$id_ms_pkpt);
                    $update = $this->mm->update_data($this->tbl_main, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["no_pkpt"] 	    = strip_tags(form_error('no_pkpt'));
            $msg_detail["thn_pkpt"] 	= strip_tags(form_error('thn_pkpt'));
            $msg_detail["nama_pkpt"] 	= strip_tags(form_error('nama_pkpt'));
            $msg_detail["tema_pkpt"] 	= strip_tags(form_error('tema_pkpt'));
            $msg_detail["kgtn_pkpt"] 	= strip_tags(form_error('kgtn_pkpt'));
            $msg_detail["hp_pkpt"] 	    = strip_tags(form_error('hp_pkpt'));
            $msg_detail["rmp_pkpt"] 	= strip_tags(form_error('rmp_pkpt'));
            $msg_detail["rpl_pkpt"] 	= strip_tags(form_error('rpl_pkpt'));
            $msg_detail["dana_pkpt"] 	= strip_tags(form_error('dana_pkpt'));
            $msg_detail["resiko_pkpt"] 	= strip_tags(form_error('resiko_pkpt'));
            $msg_detail["ket_pkpt"] 	= strip_tags(form_error('ket_pkpt'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

public function delete_data(){
    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
    $msg_detail = array(
                "id_ms_pkpt"=>"",
                
            );

    if($_POST["id_ms_pkpt"]){
        $id_ms_pkpt = $this->input->post("id_ms_pkpt");
        $where = array("id_ms_pkpt"=>$id_ms_pkpt);

        $set = array("is_del_ms_pkpt"=>"1");

        // $delete_admin = $this->mm->delete_data($this->tbl_main, $where);
        $delete_admin = $this->mm->update_data($this->tbl_main, $set, $where);
        
        if($delete_admin){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
    }else{
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail["id_ms_pkpt"]= strip_tags(form_error('id_ms_pkpt'));        
    }

    $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_pkpt"=>"0"));
    $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
    print_r(json_encode($res_msg));
}
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================
    public function check_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_ms_pkpt"=>"",
                    
                );

        if($_POST["id_ms_pkpt"]){
            $id_ms_pkpt = $this->input->post("id_ms_pkpt");
            $param = $this->input->post("param");

            $where 	= array("id_ms_pkpt"=>$id_ms_pkpt);
            if ($param == "non_active") {
                $set 	= array("sts_ac_ms_pkpt"=>"0");
            }else{
                $set 	= array("sts_ac_ms_pkpt"=>"1");
            }
            
            
            $update_data = $this->mm->update_data($this->tbl_main, $set, $where);
            if($update_data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_ms_pkpt"]= strip_tags(form_error('id_ms_pkpt'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_pkpt"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
}
