<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distrikmaster extends CI_Controller {
    public $title = "Kel dan Kel";
    public $tbl_main = "ms_distrik_pem";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "distrik";
        $data["title"] = $this->title;
		$data["list_data"] = $this->cr->tabel_self($this->tbl_main, ["id_distrik_pem", "distrik_pem_id", "nama_distrik_pem"])->result();
        // echo '<pre>';
        // var_dump($data["list_data"]);
		// echo '</pre>';
        // die();
        // $data["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("id_distrik_pem !="=>"0", "is_del_pem"=>"0"));
        $data["select_kec"] = $this->mm->get_data_all_where($this->tbl_main, array("distrik_pem_id"=>"0", "nama_distrik_pem !="=> "KOTA MALANG"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
    public function get_kec(){
        // print_r($_GET);
        $param_kec = "";
        $opsi_kel = "<option value='' selected>Pilih Kelurahan</option>";
        if(isset($_GET["param_kec"])){
                $param_kec = $_GET["param_kec"];
        }

        //kelurahan
        $data_kec = $this->mm->get_data_all_where($this->tbl_main, ["distrik_pem_id"=>$param_kec]);
        foreach ($data_kec as $key => $value) {
            $id_distrik_pem = $value->id_distrik_pem;
            $nama_distrik_pem = $value->nama_distrik_pem;
            $opsi_kel .= "<option value='".$id_distrik_pem."'>".$nama_distrik_pem."</option>";
        }
        print_r($opsi_kel);
    }

    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_distrik_pem"])){
        	$id_distrik_pem = $this->input->post('id_distrik_pem');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_distrik_pem"=>$id_distrik_pem));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'distrik_pem_id',
                'label'=>'distrik_pem_id',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'kd_distrik_pem',
                'label'=>'kd_distrik_pem',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'nama_distrik_pem',
                'label'=>'nama_distrik_pem',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'ket_distrik_pem',
                'label'=>'ket_distrik_pem',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'jenis',
                'label'=>'jenis',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "distrik_pem_id"=>"",
                    "kd_distrik_pem"=>"",
                    "nama_distrik_pem"=>"",
                    "ket_distrik_pem"=>"",
                    "jenis"=>"",
                );

        if($this->val_form_insert()){
            $jenis 	= $this->input->post("jenis", true);
            if($jenis == "kecamatan"){
                $distrik_pem_id = "0";
            }else{
                $distrik_pem_id 	= $this->input->post("distrik_pem_id", true);
            }
            $kd_distrik_pem 			= $this->input->post("kd_distrik_pem", true);
            $nama_distrik_pem 		= $this->input->post("nama_distrik_pem", true);
            $ket_distrik_pem 	= $this->input->post("ket_distrik_pem", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $distrik_pem_id],
                                [$type_pattern, $kd_distrik_pem],
                                [$type_pattern, $nama_distrik_pem],
                                [$type_pattern, $ket_distrik_pem],
                                [$type_pattern, $jenis],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data($this->tbl_main, ["nama_distrik_pem","kd_distrik_pem"], [$nama_distrik_pem,$kd_distrik_pem]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nama / Kode Sudah Terdaftar pada Sistem");
                }else{
                    $data = [
                        "id_distrik_pem"=>"",
                        "distrik_pem_id"=>$distrik_pem_id,
                        "kd_distrik_pem"=>$kd_distrik_pem,
                        "nama_distrik_pem"=>$nama_distrik_pem,
                        "ket_distrik_pem"=>$ket_distrik_pem,
                        "sts_distrik_pem"=>"1"
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["distrik_pem_id"]= strip_tags(form_error('distrik_pem_id'));
            $msg_detail["kd_distrik_pem"] 		= strip_tags(form_error('kd_distrik_pem'));
            $msg_detail["nama_distrik_pem"] 	= strip_tags(form_error('nama_distrik_pem'));
            $msg_detail["ket_distrik_pem"] 	= strip_tags(form_error('ket_distrik_pem')); 
            $msg_detail["jenis"] 	= strip_tags(form_error('jenis'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "distrik_pem_id"=>"",
                    "kd_distrik_pem"=>"",
                    "nama_distrik_pem"=>"",
                    "ket_distrik_pem"=>"",
                    "jenis"=>"",
                );

        if($this->val_form_insert()){
            $id_distrik_pem 		= $this->input->post("id_distrik_pem", true);

            $jenis 	= $this->input->post("jenis", true);
            if($jenis == "kecamatan"){
                $distrik_pem_id = "0";
            }else{
                $distrik_pem_id 	= $this->input->post("distrik_pem_id", true);
            }
            $kd_distrik_pem 			= $this->input->post("kd_distrik_pem", true);
            $nama_distrik_pem 		= $this->input->post("nama_distrik_pem", true);
            $ket_distrik_pem 	= $this->input->post("ket_distrik_pem", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $distrik_pem_id],
                                [$type_pattern, $kd_distrik_pem],
                                [$type_pattern, $nama_distrik_pem],
                                [$type_pattern, $ket_distrik_pem],
                                [$type_pattern, $jenis],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data_up($this->tbl_main, ["id_distrik_pem","nama_distrik_pem","kd_distrik_pem"], [$id_distrik_pem,$nama_distrik_pem,$kd_distrik_pem]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Nama / Kode Sudah Terdaftar pada Sistem");
                }else{
                    $set = [
                        "distrik_pem_id"=>$distrik_pem_id,
                        "kd_distrik_pem"=>$kd_distrik_pem,
                        "nama_distrik_pem"=>$nama_distrik_pem,
                        "ket_distrik_pem"=>$ket_distrik_pem
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $where = array("id_distrik_pem"=>$id_distrik_pem);
                    $update = $this->mm->update_data($this->tbl_main, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["distrik_pem_id"]= strip_tags(form_error('distrik_pem_id'));
            $msg_detail["kd_distrik_pem"] 		= strip_tags(form_error('kd_distrik_pem'));
            $msg_detail["nama_distrik_pem"] 	= strip_tags(form_error('nama_distrik_pem'));
            $msg_detail["ket_distrik_pem"] 	= strip_tags(form_error('ket_distrik_pem')); 
            $msg_detail["jenis"] 	= strip_tags(form_error('jenis'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

public function delete_data(){
    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
    $msg_detail = array(
                "id_distrik_pem"=>"",
            );

    if($_POST["id_distrik_pem"]){
        $id_distrik_pem = $this->input->post("id_distrik_pem");
        $where = array("id_distrik_pem"=>$id_distrik_pem);

        $set = array("is_del_pem"=>"1");

        // $delete_admin = $this->mm->delete_data($this->tbl_main, $where);
        $delete_admin = $this->mm->update_data($this->tbl_main, $set, $where);
        
        if($delete_admin){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
    }else{
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail["id_distrik_pem"]= strip_tags(form_error('id_distrik_pem'));        
    }

    $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("id_distrik_pem !="=>"0", "is_del_pem"=>"0"));
    $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
    print_r(json_encode($res_msg));
}
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================
    public function check_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_distrik_pem"=>"",
                );

        if($_POST["id_distrik_pem"]){
            $id_distrik_pem = $this->input->post("id_distrik_pem");
            $param = $this->input->post("param");

            $where 	= array("id_distrik_pem"=>$id_distrik_pem);
            if ($param == "non_active") {
                $set 	= array("sts_distrik_pem"=>"0");
            }else{
                $set 	= array("sts_distrik_pem"=>"1");
            }
            
            
            $update_data = $this->mm->update_data($this->tbl_main, $set, $where);
            if($update_data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_distrik_pem"]= strip_tags(form_error('id_distrik_pem'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("id_distrik_pem !="=>"0", "is_del_pem"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
}
