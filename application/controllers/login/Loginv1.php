<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'third_party/Mainjwt.php';

class Loginv1 extends CI_Controller {
    private $app_name = "APP_DAS";
    private $app_class = "LOGIN";
    private $app_func;
    
    public $tb_main = "appl_u_user";
    public $tbl_log_sess = "log_session";

    public $main_jwt;

	public function __construct(){
		parent::__construct();
		$this->load->model('main/Mainmodelaccessnew', 'am');

		$this->load->library("response_message");
		$this->load->library("Auth_v0");

        $this->main_jwt = new Mainjwt();
        // header('Content-Type: application/json');

        $this->check_auth();
	}

    private function check_auth(){
        $coockie = $this->main_jwt->get_coockie();
        if($coockie){
            if(isset($coockie["sts"])){
                if($coockie["sts"]){
                    redirect(base_url()."test_jwt/testhomejwt");
                }
            }
        }else{
            
        }
    }

    public function test(){
        print_r("ok");
    }

	public function index(){
        $data["page"] = "login_v1";
		$this->load->view('login/login_v1', $data);
	}

	private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function get_auth(){
		$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
        $msg_detail = array("username" => "",
                            "pass" => "");
        if($this->val_form_log()){
        	$username = $this->input->post('username', true);
        	$password = $this->input->post('password', true);
            
            $cek = $this->am->select_admin($username, hash("sha256", $password));

            if($cek){
                $cek["login"] = "cie_bisa_lihat";
                $cek["status_log"] = true;
                $cek["time_stamp"] = date("yyyymmddHis");
                $cek["id_session"] = md5(rand(0,2000000));
               
                $data_ins_log = $this->get_header($cek["id_appl_u_user"], $cek["id_session"]);
                $insert_log = $this->am->insert_data($this->tbl_log_sess, $data_ins_log);
                if($insert_log){
                    echo $this->main_jwt->set_coockie($cek);
                }
                // print_r($get_header);
                // $this->main_jwt->set_coockie($cek);
    		}else{
                $cek["status_log"] = false;
            }
        }else {
        	$msg_detail["username"] = form_error("username");
            $msg_detail["pass"] = form_error("pass");

            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        // print_r(json_encode($msg_array));
	}

    private function get_header($id_appl_u_user, $id_session){
        // print_r($_SERVER);
        $ip_client = $_SERVER['REMOTE_ADDR'];
        $broswer = $_SERVER['HTTP_USER_AGENT'];
       
        return [
            "id_log_session"=>"",
            "id_appl_u_user"=>$id_appl_u_user,
            "browser_log_session"=>$broswer,
            "ip_client_log_session"=>$ip_client,
            "id_client_log_session"=>$id_session,
            "exp_log_session"=>date("Y-m-d H:i:s", strtotime("+1 hours")),
            "sts_log_session"=>"0"
        ];
    }
}
