<!-- <form method="get" action="<?=base_url();?>admin/data_image_list"> -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Master List Image</h3>
        </div>
        <div class="col-md-1 text-right">
            <button type="submit" class="btn btn-info" id="form_add_image">Filter</button>
        </div>
        <div class="col-md-4">
            <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="cate" name="cate">
                <option value="default">default</option>
                <?php
                    if(isset($list_cate)){
                        if($list_cate){
                            foreach ($list_cate as $key => $value) {
                                print_r("<option value=\"".$value->ct."\">".$value->ct."</option>");
                            }
                        }
                    }
                ?>
            </select>
        </div>
        <div class="col-md-2 text-right">
            <button class="btn btn-info" id="btn_add_image">Tambah Gambar</button>
        </div>
    </div>
<!-- </form> -->
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <?php
        $path_img = base_url()."assets/prw/img/main/";
    ?>
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="row el-element-overlay">

                            <?php 
                            if(isset($list_data)){
                                foreach ($list_data as $key => $value) {
                                    $id_ms_img = $value->id_ms_img;
                                    $nm_ms_img = $value->nm_ms_img;
                                    $ket_ms_img = $value->ket_ms_img;
                                    $src_ms_img = $value->src_ms_img;
                                    $tag_ms_img = $value->tag_ms_img;

                                    $title_img_show = $nm_ms_img;
                                    if(strlen($nm_ms_img) < 30){
                                        $title_img_show = substr($nm_ms_img, 0, 30)." ...";
                                    }

                                    print_r("<div class=\"col-lg-3 col-md-6\">
                                                <div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">
                                                    <div class=\"el-card-item\" style=\"padding: 0px;\">
                                                        <div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\"> <img src=\"".$path_img.$src_ms_img."\" alt=\"user\">
                                                            <div class=\"el-overlay\">
                                                                <ul class=\"el-info\">
                                                                    <li><a class=\"btn default btn-outline image-popup-vertical-fit\" href=\"".$path_img.$src_ms_img."\"><i class=\"icon-magnifier\"></i></a></li>
                                                                    <li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"copy_clip('".$path_img.$src_ms_img."')\"><i class=\"icon-link\"></i></a></li>
                                                                    <li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"del_main('".$id_ms_img."')\"><i class=\"icon-trash\"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div style=\"padding: 2px;\">
                                                            <b><label>".$tag_ms_img."</label></b>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>");
                                }
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- --------------------------insert_data------------------------  -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Image<span style="color: red;"> (max. 1.2 Mb, ukuran terbaik 2362px X 1329px) *</span></label>
                                    <input type="file" class="form-control" id="src_ms_img" name="src_ms_img" required="">
                                    <p id="msg_src_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <img id="out_src_ms_img" src="" alt="" style="width:500px;350px">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama Img<span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required="">
                                    <p id="msg_nm_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Keterangan</label>
                                    <textarea class="form-control" id="ket_ms_img" name="ket_ms_img" required="" cols="30" rows="10"></textarea>
                                    <p id="msg_ket_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Tag<span style="color: red;"> (untuk memudahkan pencarian isikane dengan pemisah ',' tanpa spasi, <br><b>contoh: pariwisata,kuliner</b>) *</span></label>
                                    <input type="text" class="form-control" id="tag_ms_img" name="tag_ms_img" required="">
                                    <p id="msg_tag_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                    </div>


                    
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button> -->
                <button type="button" id="btn_save_data" class="btn btn-info waves-effect text-left">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_data------------------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------Update_data------------------------  -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" id="modal_up" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 70%;">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Update Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Image<span style="color: red;"> (max. 1.2 Mb, ukuran terbaik 2362px X 1329px) *</span></label>
                                    <input type="file" class="form-control" id="_src_ms_img" name="src_ms_img" required="">
                                    <p id="_msg_src_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <img id="_out_src_ms_img" src="" alt="" style="width:300px;200px">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama Img<span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" id="_nm_ms_img" name="nm_ms_img" required="">
                                    <p id="_msg_nm_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Keterangan</label>
                                    <textarea class="form-control" id="_ket_ms_img" name="ket_ms_img" required="" cols="30" rows="10"></textarea>
                                    <p id="_msg_ket_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Tag<span style="color: red;"> (untuk memudahkan pencarian isikane dengan pemisah ',' tanpa spasi, <br><b>contoh: pariwisata,kuliner</b>) *</span></label>
                                    <input type="text" class="form-control" id="_tag_ms_img" name="tag_ms_img" required="">
                                    <p id="_msg_tag_ms_img" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                    </div>


                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" id="btn_up_jenis" class="btn btn-info waves-effect text-left">Ubah</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- --------------------------Update_data------------------------  -->
<!-- ============================================================== -->
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>
<script type="text/javascript">

    var max_size = 1500000;
    var width = 2362;
    var height = 1329;

    var tmp_img = [];
    
    // var main_url_ajax = "";

    $(document).ready(function(){

    });

    //=========================================================================//
    //-----------------------------------imsg_change---------------------------//
    //=========================================================================//
        function readURL(input) {
            // console.log(input.files[0])
            var arr_req_type_data = ["image/jpeg", "image/jpg", "image/png"];
            var max_size = 1000000;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                function ckType(type) {
                    return type >= input.files[0].type;
                }


                if(arr_req_type_data.find(ckType) && input.files[0].size <= max_size){
                    reader.onload = function (e) {
                        $('#out_src_ms_img').attr('src', e.target.result);
                        tmp_img = input.files[0];
                    }

                }else{
                    create_alert("Warning", "Pastikan format dan ukuran data sesuai ketentuan", "error");
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#src_ms_img").change(function(){
            readURL(this);
        });

        function readURLUp(input) {
            // console.log(input.files[0])
            var arr_req_type_data = ["image/jpeg", "image/jpg", "image/png"];
            var max_size = 1000000;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                function ckType(type) {
                    return type >= input.files[0].type;
                }


                if(arr_req_type_data.find(ckType) && input.files[0].size <= max_size){
                    reader.onload = function (e) {
                        $('#_out_src_ms_img').attr('src', e.target.result);
                        tmp_img = input.files[0];
                    }

                }else{
                    create_alert("Warning", "Pastikan format dan ukuran data sesuai ketentuan", "error");
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#_src_ms_img").change(function(){
            readURLUp(this);
        });
    //=========================================================================//
    //-----------------------------------imsg_change---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------add_img-------------------------------//
    //=========================================================================//
        $("#btn_add_image").click(function() {
            $('#modal_add').modal('toggle');
        });


        $("#btn_save_data").click(function() {
            var data_main = new FormData();
            data_main.append('nm_ms_img', $("#nm_ms_img").val());
            data_main.append('ket_ms_img', $("#ket_ms_img").val());
            data_main.append('src_ms_img', tmp_img);
            data_main.append('tag_ms_img', $("#tag_ms_img").val());
            
            $.ajax({
                url: "<?php echo base_url()."admin/mainimg/insert_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                // $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/master-img");?>");
            } else {
                $("#msg_nm_ms_img").html(detail_msg.nm_ms_img);
                $("#msg_ket_ms_img").html(detail_msg.ket_ms_img);
                $("#msg_src_ms_img").html(detail_msg.src_ms_img);
                $("#msg_tag_ms_img").html(detail_msg.tag_ms_img);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#msg_nm_ms_img").html("");
            $("#msg_ket_ms_img").html("");
            $("#msg_src_ms_img").html("");
            $("#msg_tag_ms_img").html("");
        }
    //=========================================================================//
    //-----------------------------------add_img-------------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_update_data-----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#msg_nm_ms_img").html("");
            $("#msg_ket_ms_img").html("");
            $("#msg_src_ms_img").html("");
            $("#msg_tag_ms_img").html("");
        }

        function update_data(id_ms_img) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_ms_img', id_ms_img);

            $.ajax({
                url: "<?php echo base_url()."admin/mainimg/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    set_val_update(res, id_ms_img);
                    $("#modal_up_jenis").modal('show');
                }
            });
        }

        function set_val_update(res, id_ms_img ) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_ms_img ;

                $("#_nm_ms_img").val(list_data.nm_ms_img);
                $("#_ket_ms_img").val(list_data.ket_ms_img);
                // $("#_src_ms_img").val(list_data.src_ms_img);
                $("#_tag_ms_img").val(list_data.tag_ms_img);

                $("#_out_src_ms_img").attr("src", path_img+list_data.src_ms_img);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_update_data-----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------copy_clip-----------------------------//
    //=========================================================================//
        function copy_clip(text){
            copyTextToClipboard(text);
        }
    //=========================================================================//
    //-----------------------------------copy_clip-----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//
        function alert_confirm(title, msg, status, txt_cencel_btn, id){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: title,
                        text: msg,
                        type: status,
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: txt_cencel_btn,
                        closeOnConfirm: false
                    }, function() {
                        del_img_func(id);
                        swal.close();
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
        
        function del_main(id){
            alert_confirm("Pesan Konfirmasi.!!", "Apa anda yakin ingin menghapus Gambar ini ?", "warning", "Hapus", id);
        }

        function del_img_func(id){
            var data_main = new FormData();
            data_main.append('id_ms_img', id);

            // console.log(id);
            $.ajax({
                url: "<?php echo base_url()."admin/mainimg/delete_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_del_fucn(res);
                }
            });
        }

        function response_del_fucn(res){
            console.log(res);
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                window.location.href = "<?php print_r(base_url());?>admin/master-img";
                // create_sweet_alert("Pesan Success!!", "Gambar ini Berhasil dihapus", "success", "");
            } else {
                create_sweet_alert("Pesan Gagal!!", "Gambar ini Gagal dihapus", "warning", "");
            }
            
        }
    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//
</script>