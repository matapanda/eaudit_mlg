<?php
    $main_controller = base_url()."master/distrikmaster/";

    $path_img = base_url()."assets/prw/img/icon/";

    // var_dump($list_data);
    $list_kecamatan= '<option value="" selected>Pilih Kecamatan</option>';
    foreach($select_kec as $row){
        $list_kecamatan .= '<option value="'.$row->id_distrik_pem.'">'.$row->nama_distrik_pem.'</option>';
    }
    // $list_encode = json_encode($list_data, true);

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor">Data List <?=$title?></h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Input Data <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis</label>
                                <select type="text" class="form-control" id="jenis" name="jenis">
                                    <option value="">Pilih Jenis</option>
                                    <option value="kecamatan">Kecamatan</option>
                                    <option value="kelurahan">Kelurahan</option>
                                </select>
                                <p id="msg_jenis" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-4 div_kec">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kecamatan</label>
                                <select type="text" class="form-control" id="distrik_pem_id" name="distrik_pem_id">
                                    <?=$list_kecamatan?>
                                </select>
                                <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                                <p id="msg_distrik_pem_id" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode</label>
                                <input type="text" class="form-control" id="kd_distrik_pem" name="kd_distrik_pem" required="">
                                <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                                <p id="msg_kd_distrik_pem" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama</label>
                                <input type="text" class="form-control" id="nama_distrik_pem" name="nama_distrik_pem" required="">
                                <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                                <p id="msg_nama_distrik_pem" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan</label>
                                <input type="text" class="form-control" id="ket_distrik_pem" name="ket_distrik_pem" required="">
                                <p id="msg_ket_distrik_pem" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <button type="button" id="save_data" class="btn btn-info waves-effect text-left">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">List <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="10%">Kode</th>
                                            <th width="15%">Nama</th>
                                            <th width="15%">Kecamatan</th>
                                            <th width="15%">Keterangan</th>
                                            <th width="15%">Status Active</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r => $v) {
                                                    $id_distrik_pem = $v->id_distrik_pem;
                                                    $kd_distrik_pem = $v->kd_distrik_pem;
                                                    $distrik_pem_id = $v->distrik_pem_id;
                                                    $nama_distrik_pem = $v->nama_distrik_pem;
                                                    $ket_distrik_pem = $v->ket_distrik_pem;
                                                    $sts_distrik_pem = $v->sts_distrik_pem;
                                                    $parent = $v->parent;
                                                    if ($parent == 'KOTA MALANG') {
                                                        $parent = "-";
                                                    }
                                                   
                                                    $str_btn_active = "<button class=\"btn btn-primary\" id=\"ac_admin\" onclick=\"check_data('".$id_distrik_pem."', 'active')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                                                    
                                                    $str_active = "<span class=\"label label-warning\">Tidak Aktif</span>";
                                                    if($sts_distrik_pem == "1"){
                                                        $str_active = "<span class=\"label label-info\">Aktif</span>";
                                                        
                                                        $str_btn_active = "<button class=\"btn btn-success\" id=\"un_ac_admin\" onclick=\"check_data('".$id_distrik_pem."', 'non_active')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                                                    }
                                                    
                                                   
                                                    echo "<tr>
                                                            <td>".($r+1)."</td>
                                                            <td>".$kd_distrik_pem."</td>
                                                            <td>".$nama_distrik_pem."</td>
                                                            <td>".$parent."</td>
                                                            <td>".$ket_distrik_pem."</td>
                                                            <td>".$str_active."</td>
                                                            <td>
                                                                <center>
                                                                ".$str_btn_active."
                                                                <button class=\"btn btn-info\" onclick=\"update_data('".$id_distrik_pem ."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;
                                                                <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$id_distrik_pem ."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                            </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="modal_up_data" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Jenis </label>
                        <select type="text" class="form-control" id="_jenis" name="_jenis">
                            <option value="" selected>Pilih Jenis</option>
                            <option value="kecamatan">Kecamatan</option>
                            <option value="kelurahan">Kelurahan</option>
                        </select>
                        <p id="_msg_jenis" style="color: red;"></p>
                    </div>
                </div>
                <div class="col-md-4 _div_kec">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Kecamatan</label>
                        <select type="text" class="form-control" id="_distrik_pem_id" name="_distrik_pem_id">
                            <?=$list_kecamatan?>
                        </select>
                        <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                        <p id="_msg_distrik_pem_id" style="color: red;"></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Kode</label>
                        <input type="text" class="form-control" id="_kd_distrik_pem" name="_kd_distrik_pem" required="">
                        <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                        <p id="_msg_kd_distrik_pem" style="color: red;"></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Nama</label>
                        <input type="text" class="form-control" id="_nama_distrik_pem" name="_nama_distrik_pem" required="">
                        <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                        <p id="_msg_nama_distrik_pem" style="color: red;"></p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Keterangan</label>
                        <input type="text" class="form-control" id="_ket_distrik_pem" name="_ket_distrik_pem" required="">
                        <p id="_msg_ket_distrik_pem" style="color: red;"></p>
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" id="up_data" class="btn btn-info waves-effect text-left">Ubah</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";

    $(document).ready(function() {
        $(".div_kec").hide()
    })

    $("#jenis").change(function() {
        if ($(this).val() == "kelurahan") {
            $("#distrik_pem_id").val("")
            $(".div_kec").show()
        }else if($(this).val() == "kecamatan"){
            $("#distrik_pem_id").val("0")
            $(".div_kec").hide()
        }else{
            $("#distrik_pem_id").val("1")
            $(".div_kec").hide()
        }
    })

    $("#_jenis").change(function() {
        if ($(this).val() == "kelurahan") {
            $("#_distrik_pem_id").val("")
            $("._div_kec").show()
        }else{
            $("#_distrik_pem_id").val("0")
            $("._div_kec").hide()
        }
    })

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#save_data").click(function() {
            var data_main = new FormData();
            data_main.append('jenis', $("#jenis").val());
            data_main.append('distrik_pem_id', $("#distrik_pem_id").val());
            data_main.append('kd_distrik_pem', $("#kd_distrik_pem").val());
            data_main.append('nama_distrik_pem', $("#nama_distrik_pem").val());
            data_main.append('ket_distrik_pem', $("#ket_distrik_pem").val());
            
            $.ajax({
                url: "<?= $main_controller."insert_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                    // console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                // $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/distrik");?>");
            } else {
                $("#msg_distrik_pem_id").html(detail_msg.distrik_pem_id);
                $("#msg_kd_distrik_pem").html(detail_msg.kd_distrik_pem);
                $("#msg_nama_distrik_pem").html(detail_msg.nama_distrik_pem);
                $("#msg_ket_distrik_pem").html(detail_msg.ket_distrik_pem);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#msg_distrik_pem_id").html("");
            $("#msg_kd_distrik_pem").html("");
            $("#msg_nama_distrik_pem").html("");
            $("#msg_ket_distrik_pem").html("");
            $("#msg_jenis").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_msg_distrik_pem_id").html("");
            $("#_msg_kd_distrik_pem").html("");
            $("#_msg_nama_distrik_pem").html("");
            $("#_msg_ket_distrik_pem").html("");
            $("#_msg_jenis").html("");
        }

        function update_data(id_distrik_pem) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_distrik_pem', id_distrik_pem);

            $.ajax({
                url: "<?= $main_controller."get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_distrik_pem);
                    $("#modal_up_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_distrik_pem ) {
            var data_json = JSON.parse(res);
            // console.log(data_json);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_distrik_pem ;
                if (list_data.distrik_pem_id == "0") {
                    $("#_jenis").val("kecamatan");
                    $("._div_kec").hide()
                }else{
                    $("#_jenis").val("kelurahan");
                    $("._div_kec").show()
                }
                $("#_distrik_pem_id").val(list_data.distrik_pem_id);
                $("#_kd_distrik_pem").val(list_data.kd_distrik_pem);
                $("#_nama_distrik_pem").val(list_data.nama_distrik_pem);
                $("#_ket_distrik_pem").val(list_data.ket_distrik_pem);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#up_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_distrik_pem', id_cache);
            data_main.append('jenis', $("#_jenis").val());
            data_main.append('distrik_pem_id', $("#_distrik_pem_id").val());
            data_main.append('kd_distrik_pem', $("#_kd_distrik_pem").val());
            data_main.append('nama_distrik_pem', $("#_nama_distrik_pem").val());
            data_main.append('ket_distrik_pem', $("#_ket_distrik_pem").val());

            $.ajax({
                url: "<?= $main_controller."update_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#modal_up_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/distrik");?>");
            } else {
                $("#_msg_distrik_pem_id").html(detail_msg.distrik_pem_id);
                $("#_msg_kd_distrik_pem").html(detail_msg.kd_distrik_pem);
                $("#_msg_nama_distrik_pem").html(detail_msg.nama_distrik_pem);
                $("#_msg_ket_distrik_pem").html(detail_msg.ket_distrik_pem);
                $("#_msg_jenis").html(detail_msg.jenis);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_distrik_pem ){
            var data_main = new FormData();
            data_main.append('id_distrik_pem', id_distrik_pem );

            $.ajax({
                url: "<?= $main_controller."delete_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_distrik_pem ) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_distrik_pem );
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/distrik");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//
        function check_data(id_distrik_pem, sts){
            var data_main = new FormData();
            data_main.append('id_distrik_pem', id_distrik_pem);
            data_main.append('param', sts);
            console.log(sts)
            $.ajax({
                url: "<?= $main_controller."check_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }
    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//

    
</script>