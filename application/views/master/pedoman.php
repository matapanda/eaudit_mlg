<?php
    $main_controller = base_url()."master/pedomanmaster/";

    $path_img = base_url()."assets/prw/img/icon/";

    // var_dump($list_data);
    $list_jenis= '<option value="" selected>Pilih Jenis</option>';
    foreach($select_jn as $row){
        $list_jenis .= '<option value="'.$row->id_ms_jn_audit.'">'.$row->nama_ms_jn_audit.'</option>';
    }

    $list_sasaran= '<option value="" selected>Pilih Sasaran</option>';
    foreach($select_ssrn as $row){
        $list_sasaran .= '<option value="'.$row->id_ms_ssrn_audit.'">'.$row->nama_ms_ssrn_audit.'</option>';
    }

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor">Data List <?=$title?></h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Input Data <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode</label>
                                <input type="text" class="form-control" id="kd_ped_audit" name="kd_ped_audit" required="">
                                <p id="msg_kd_ped_audit" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-4 div_kec">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Audit</label>
                                <select type="text" class="form-control" id="ms_jn_audit_id" name="ms_jn_audit_id">
                                    <?=$list_jenis?>
                                </select>
                                <p id="msg_ms_jn_audit_id" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Sasaran Audit</label>
                                <select type="text" class="form-control" id="ms_ssrn_audit_id" name="ms_ssrn_audit_id">
                                    <?=$list_sasaran?>
                                </select>
                                <p id="msg_ms_ssrn_audit_id" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <button type="button" id="save_data" class="btn btn-info waves-effect text-left">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">List <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="10%">Kode</th>
                                            <th width="15%">Jenis Audit</th>
                                            <th width="15%">Sasaran Audit</th>
                                            <th width="15%">Status Active</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r => $v) {
                                                    $id_ms_ped_audit = $v->id_ms_ped_audit;
                                                    $kd_ped_audit = $v->kd_ped_audit;
                                                    $ms_jn_audit_id = $v->ms_jn_audit_id;
                                                    $ms_ssrn_audit_id = $v->ms_ssrn_audit_id;
                                                    $sts_ac_ms_ped_audit = $v->sts_ac_ms_ped_audit;
                                                    $nm_jenis = $v->nm_jenis;
                                                    $nm_sasaran = $v->nm_sasaran;
                                                   
                                                    $str_btn_active = "<button class=\"btn btn-primary\" id=\"ac_admin\" onclick=\"check_data('".$id_ms_ped_audit."', 'active')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                                                    
                                                    $str_active = "<span class=\"label label-warning\">Tidak Aktif</span>";
                                                    if($sts_ac_ms_ped_audit == "1"){
                                                        $str_active = "<span class=\"label label-info\">Aktif</span>";
                                                        
                                                        $str_btn_active = "<button class=\"btn btn-success\" id=\"un_ac_admin\" onclick=\"check_data('".$id_ms_ped_audit."', 'non_active')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                                                    }
                                                    
                                                   
                                                    echo "<tr>
                                                            <td>".($r+1)."</td>
                                                            <td>".$kd_ped_audit."</td>
                                                            <td>".$nm_jenis."</td>
                                                            <td>".$nm_sasaran."</td>
                                                            <td>".$str_active."</td>
                                                            <td>
                                                                <center>
                                                                ".$str_btn_active."
                                                                <button class=\"btn btn-info\" onclick=\"update_data('".$id_ms_ped_audit ."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;
                                                                <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$id_ms_ped_audit ."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                            </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="modal_up_data" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Kode</label>
                        <input type="text" class="form-control" id="_kd_ped_audit" name="_kd_ped_audit" required="">
                        <p id="_msg_kd_ped_audit" style="color: red;"></p>
                    </div>
                </div>
                <div class="col-md-4 _div_kec">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Jenis Audit</label>
                        <select type="text" class="form-control" id="_ms_jn_audit_id" name="_ms_jn_audit_id">
                            <?=$list_jenis?>
                        </select>
                        <p id="_msg_ms_jn_audit_id" style="color: red;"></p>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Sasaran Audit</label>
                        <select type="text" class="form-control" id="_ms_ssrn_audit_id" name="_ms_ssrn_audit_id">
                            <?=$list_sasaran?>
                        </select>
                        <p id="_msg_ms_ssrn_audit_id" style="color: red;"></p>
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" id="up_data" class="btn btn-info waves-effect text-left">Ubah</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";

    $(document).ready(function() {
       
    })

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#save_data").click(function() {
            var data_main = new FormData();
            data_main.append('ms_jn_audit_id', $("#ms_jn_audit_id").val());
            data_main.append('kd_ped_audit', $("#kd_ped_audit").val());
            data_main.append('ms_ssrn_audit_id', $("#ms_ssrn_audit_id").val());
            
            $.ajax({
                url: "<?= $main_controller."insert_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                    // console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                // $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/pedoman");?>");
            } else {
                $("#msg_ms_jn_audit_id").html(detail_msg.ms_jn_audit_id);
                $("#msg_kd_ped_audit").html(detail_msg.kd_ped_audit);
                $("#msg_ms_ssrn_audit_id").html(detail_msg.ms_ssrn_audit_id);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#msg_ms_jn_audit_id").html("");
            $("#msg_kd_ped_audit").html("");
            $("#msg_ms_ssrn_audit_id").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_msg_ms_jn_audit_id").html("");
            $("#_msg_kd_ped_audit").html("");
            $("#_msg_ms_ssrn_audit_id").html("");
        }

        function update_data(id_ms_ped_audit) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_ms_ped_audit', id_ms_ped_audit);

            $.ajax({
                url: "<?= $main_controller."get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_ms_ped_audit);
                    $("#modal_up_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_ms_ped_audit ) {
            var data_json = JSON.parse(res);
            // console.log(data_json);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_ms_ped_audit ;
                $("#_ms_jn_audit_id").val(list_data.ms_jn_audit_id);
                $("#_kd_ped_audit").val(list_data.kd_ped_audit);
                $("#_ms_ssrn_audit_id").val(list_data.ms_ssrn_audit_id);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#up_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_ms_ped_audit', id_cache);
            data_main.append('ms_jn_audit_id', $("#_ms_jn_audit_id").val());
            data_main.append('kd_ped_audit', $("#_kd_ped_audit").val());
            data_main.append('ms_ssrn_audit_id', $("#_ms_ssrn_audit_id").val());

            $.ajax({
                url: "<?= $main_controller."update_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#modal_up_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/pedoman");?>");
            } else {
                $("#_msg_ms_jn_audit_id").html(detail_msg.ms_jn_audit_id);
                $("#_msg_kd_ped_audit").html(detail_msg.kd_ped_audit);
                $("#_msg_ms_ssrn_audit_id").html(detail_msg.ms_ssrn_audit_id);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_ms_ped_audit ){
            var data_main = new FormData();
            data_main.append('id_ms_ped_audit', id_ms_ped_audit );

            $.ajax({
                url: "<?= $main_controller."delete_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_ms_ped_audit ) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_ms_ped_audit );
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/pedoman");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//
        function check_data(id_ms_ped_audit, sts){
            var data_main = new FormData();
            data_main.append('id_ms_ped_audit', id_ms_ped_audit);
            data_main.append('param', sts);
            console.log(sts)
            $.ajax({
                url: "<?= $main_controller."check_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }
    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//

    
</script>