<?php
    $main_controller = base_url()."master/pkptmaster/";

    $path_img = base_url()."assets/prw/img/icon/";

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor">Data List <?=$title?></h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Input Data <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">No <?=$title?></label>
                                <div class="input-group">
                                    <!-- <input type="text" class="form-control" id="thn_pkpt" name="thn_pkpt" placeholder="Tahun.." required maxlength="4" style="margin-right: 4px" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"> -->
                                        <select type="text" class="form-control" id="thn_pkpt" name="thn_pkpt">
                                            <option value="">Pilih Tahun</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                        </select>
                                    <input type="text" class="form-control" id="no_pkpt" name="no_pkpt" placeholder="Nomor.." required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" >                                    
                                </div>
                                <p id="msg_thn_pkpt" style="color: red;"></p>
                                <p id="msg_no_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama <?=$title?></label>
                                <input type="text" class="form-control" id="nama_pkpt" name="nama_pkpt" required placeholder="Nama PKPT">
                                <p id="msg_nama_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tema <?=$title?></label>
                                <input type="text" class="form-control" id="tema_pkpt" name="tema_pkpt" required="" placeholder="Tema PKPT">
                                <p id="msg_tema_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kegiatan <?=$title?></label>
                                <input type="text" class="form-control" id="kgtn_pkpt" name="kgtn_pkpt" required="" placeholder="Kegiatan PKPT">
                                <p id="msg_kgtn_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">HP </label>
                                <input type="text" class="form-control" id="hp_pkpt" name="hp_pkpt" required="" placeholder="HP" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                                <p id="msg_hp_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">RMP </label>
                                <input type="text" class="form-control" id="rmp_pkpt" name="rmp_pkpt" required="" placeholder="RMP"> 
                                <p id="msg_rmp_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">RPL </label>
                                <input type="text" class="form-control" id="rpl_pkpt" name="rpl_pkpt" required="" placeholder="RPL"> 
                                <p id="msg_rpl_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Dana </label>
                                <input type="text" class="form-control" id="dana_pkpt" name="dana_pkpt" required="" placeholder="Dana">
                                <p id="msg_dana_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Resiko </label>
                                <input type="text" class="form-control" id="resiko_pkpt" name="resiko_pkpt" required="" placeholder="Resiko">
                                <p id="msg_resiko_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan </label>
                                <input type="text" class="form-control" id="ket_pkpt" name="ket_pkpt" required="" placeholder="Keterangan">
                                <p id="msg_ket_pkpt" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-12 text-right">
                            <button type="button" id="save_data" class="btn btn-info waves-effect text-left">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">List <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="15%">No <?=$title?></th>
                                            <th width="15%">Nama  <?=$title?></th>
                                            <th width="15%">Tema <?=$title?></th>
                                            <th width="15%">Kegiatan  <?=$title?></th>
                                            <th width="15%">Status Active</th>
                                            <th width="30%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r => $v) {
                                                    $id_ms_pkpt = $v->id_ms_pkpt;
                                                    $no_pkpt = $v->no_pkpt;
                                                    $thn_pkpt = $v->thn_pkpt;
                                                    $nama_pkpt = $v->nama_pkpt;
                                                    $tema_pkpt = $v->tema_pkpt;
                                                    $kgtn_pkpt = $v->kgtn_pkpt;
                                                    $hp_pkpt = $v->hp_pkpt;
                                                    $rmp_pkpt = $v->rmp_pkpt;
                                                    $rpl_pkpt = $v->rpl_pkpt;
                                                    $dana_pkpt = $v->dana_pkpt;
                                                    $resiko_pkpt = $v->resiko_pkpt;
                                                    $ket_pkpt = $v->ket_pkpt;
                                                    $sts_ac_ms_pkpt = $v->sts_ac_ms_pkpt;

                                                    $str_btn_active = "<button class=\"btn btn-primary\" id=\"ac_admin\" onclick=\"check_data('".$id_ms_pkpt."', 'active')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                                                    
                                                    $str_active = "<span class=\"label label-warning\">Tidak Aktif</span>";
                                                    if($sts_ac_ms_pkpt == "1"){
                                                        $str_active = "<span class=\"label label-info\">Aktif</span>";
                                                        
                                                        $str_btn_active = "<button class=\"btn btn-success\" id=\"un_ac_admin\" onclick=\"check_data('".$id_ms_pkpt."', 'non_active')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                                                    }
                                                    
                                                   
                                                    echo "<tr>
                                                            <td>".($r+1)."</td>
                                                            <td>".$no_pkpt."</td>
                                                            <td>".$nama_pkpt."</td>
                                                            <td>".$tema_pkpt."</td>
                                                            <td>".$kgtn_pkpt."</td>
                                                            <td>".$str_active."</td>
                                                            <td>
                                                                <center>
                                                                ".$str_btn_active."
                                                                <button class=\"btn btn-info\" onclick=\"update_data('".$id_ms_pkpt ."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;
                                                                <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$id_ms_pkpt."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                            </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="modal_up_data" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">No <?=$title?></label>
                        <div class="input-gorup">
                            <!-- <input type="text" class="form-control" id="_thn_pkpt" name="_thn_pkpt" placeholder="Tahun.." required maxlength="4" style="margin-right: 4px" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">  -->
                                <select type="text" class="form-control" id="_thn_pkpt" name="_thn_pkpt">
                                            <option value="">Pilih Tahun</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                </select>
                            <input type="text" class="form-control" id="_no_pkpt" name="_no_pkpt" placeholder="Nomor.." required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" >
                        </div>
                        <p id="_msg_no_pkpt" style="color: red;"></p>
                        <p id="_msg_thn_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Nama <?=$title?></label>
                        <input type="text" class="form-control" id="_nama_pkpt" name="_nama_pkpt" required="">
                        <p id="_msg_nama_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Tema <?=$title?></label>
                        <input type="text" class="form-control" id="_tema_pkpt" name="_tema_pkpt" required="">
                        <p id="_msg_tema_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Kegiatan <?=$title?></label>
                        <input type="text" class="form-control" id="_kgtn_pkpt" name="_kgtn_pkpt" required="">
                        <p id="_msg_kgtn_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">HP </label>
                        <input type="text" class="form-control" id="_hp_pkpt" name="_hp_pkpt" required="">
                        <p id="_msg_hp_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">RMP </label>
                        <input type="text" class="form-control" id="_rmp_pkpt" name="_rmp_pkpt" required="">
                        <p id="_msg_rmp_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">RPL </label>
                        <input type="text" class="form-control" id="_rpl_pkpt" name="_rpl_pkpt" required="">
                        <p id="_msg_rpl_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Dana</label>
                        <input type="text" class="form-control" id="_dana_pkpt" name="_dana_pkpt" required="">
                        <p id="_msg_dana_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Resiko</label>
                        <input type="text" class="form-control" id="_resiko_pkpt" name="_resiko_pkpt" required="">
                        <p id="_msg_resiko_pkpt" style="color: red;"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Keterangan </label>
                        <input type="text" class="form-control" id="_ket_pkpt" name="_ket_pkpt" required="">
                        <p id="_msg_ket_pkpt" style="color: red;"></p>
                    </div>
                </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" id="up_data" class="btn btn-info waves-effect text-left">Ubah</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";

    $(document).ready(function() {
        
    })

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#save_data").click(function() {
            // console.log($("#thn_pkpt").val())

            var data_main = new FormData();
            data_main.append('no_pkpt', $("#no_pkpt").val());
            data_main.append('thn_pkpt', $("#thn_pkpt").val());
            data_main.append('tema_pkpt', $("#tema_pkpt").val());
            data_main.append('nama_pkpt', $("#nama_pkpt").val());
            data_main.append('kgtn_pkpt', $("#kgtn_pkpt").val());
            data_main.append('hp_pkpt', $("#hp_pkpt").val());
            data_main.append('rmp_pkpt', $("#rmp_pkpt").val());
            data_main.append('rpl_pkpt', $("#rpl_pkpt").val());
            data_main.append('dana_pkpt', $("#dana_pkpt").val());
            data_main.append('resiko_pkpt', $("#resiko_pkpt").val());
            data_main.append('ket_pkpt', $("#ket_pkpt").val());
            
            $.ajax({
                url: "<?= $main_controller."insert_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                    // console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                // $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/pkpt");?>");
            } else {
                $("#msg_no_pkpt").html(detail_msg.no_pkpt);
                $("#msg_thn_pkpt").html(detail_msg.thn_pkpt);
                $("#msg_tema_pkpt").html(detail_msg.tema_pkpt);
                $("#msg_nama_pkpt").html(detail_msg.nama_pkpt);
                $("#msg_kgtn_pkpt").html(detail_msg.kgtn_pkpt);
                $("#msg_hp_pkpt").html(detail_msg.hp_pkpt);
                $("#msg_rmp_pkpt").html(detail_msg.rmp_pkpt);
                $("#msg_rpl_pkpt").html(detail_msg.rpl_pkpt);
                $("#msg_dana_pkpt").html(detail_msg.dana_pkpt);
                $("#msg_resiko_pkpt").html(detail_msg.resiko_pkpt);
                $("#msg_ket_pkpt").html(detail_msg.ket_pkpt);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#msg_no_pkpt").html("");
            $("#msg_thn_pkpt").html("");
            $("#msg_tema_pkpt").html("");
            $("#msg_nama_pkpt").html("");
            $("#msg_kgtn_pkpt").html("");
            $("#msg_hp_pkpt").html("");
            $("#msg_rmp_pkpt").html("");
            $("#msg_rpl_pkpt").html("");
            $("#msg_dana_pkpt").html("");
            $("#msg_resiko_pkpt").html("");
            $("#msg_ket_pkpt").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_msg_no_pkpt").html("");
            $("#_msg_thn_pkpt").html("");
            $("#_msg_tema_pkpt").html("");
            $("#_msg_nama_pkpt").html("");
            $("#_msg_kgtn_pkpt").html("");
            $("#_msg_hp_pkpt").html("");
            $("#_msg_rmp_pkpt").html("");
            $("#_msg_rpl_pkpt").html("");
            $("#_msg_dana_pkpt").html("");
            $("#_msg_resiko_pkpt").html("");
            $("#_msg_ket_pkpt").html("");
        }

        function update_data(id_ms_pkpt) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_ms_pkpt', id_ms_pkpt);

            $.ajax({
                url: "<?= $main_controller."get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_ms_pkpt);
                    $("#modal_up_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_ms_pkpt ) {
            var data_json = JSON.parse(res);
            console.log(data_json);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_ms_pkpt ;
                let no_raw = list_data.no_pkpt;
                let no_asli = no_raw.substring(4);          

                $("#_no_pkpt").val(no_asli);
                $("#_thn_pkpt").val(list_data.thn_pkpt);
                $("#_tema_pkpt").val(list_data.tema_pkpt);
                $("#_nama_pkpt").val(list_data.nama_pkpt);
                $("#_kgtn_pkpt").val(list_data.kgtn_pkpt);
                $("#_hp_pkpt").val(list_data.hp_pkpt);
                $("#_rmp_pkpt").val(list_data.rmp_pkpt);
                $("#_rpl_pkpt").val(list_data.rpl_pkpt);
                $("#_dana_pkpt").val(list_data.dana_pkpt);
                $("#_resiko_pkpt").val(list_data.resiko_pkpt);
                $("#_ket_pkpt").val(list_data.ket_pkpt);
            }else {
                clear_form_update();
            }
        }

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#up_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_ms_pkpt', id_cache);
            data_main.append('no_pkpt', $("#_no_pkpt").val());
            data_main.append('thn_pkpt', $("#_thn_pkpt").val());
            data_main.append('tema_pkpt', $("#_tema_pkpt").val());
            data_main.append('nama_pkpt', $("#_nama_pkpt").val());
            data_main.append('kgtn_pkpt', $("#_kgtn_pkpt").val());
            data_main.append('hp_pkpt', $("#_hp_pkpt").val());
            data_main.append('rmp_pkpt', $("#_rmp_pkpt").val());
            data_main.append('rpl_pkpt', $("#_rpl_pkpt").val());
            data_main.append('dana_pkpt', $("#_dana_pkpt").val());
            data_main.append('resiko_pkpt', $("#_resiko_pkpt").val());
            data_main.append('ket_pkpt', $("#_ket_pkpt").val());

            $.ajax({
                url: "<?= $main_controller."update_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#modal_up_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/pkpt");?>");
            } else {
                $("#_msg_no_pkpt").html(detail_msg.no_pkpt);
                $("#_msg_thn_pkpt").html(detail_msg.thn_pkpt);
                $("#_msg_tema_pkpt").html(detail_msg.tema_pkpt);
                $("#_msg_nama_pkpt").html(detail_msg.nama_pkpt);
                $("#_msg_kgtn_pkpt").html(detail_msg.kgtn_pkpt);
                $("#_msg_hp_pkpt").html(detail_msg.hp_pkpt);
                $("#_msg_rmp_pkpt").html(detail_msg.rmp_pkpt);
                $("#_msg_rpl_pkpt").html(detail_msg.rpl_pkpt);
                $("#_msg_dana_pkpt").html(detail_msg.dana_pkpt);
                $("#_msg_resiko_pkpt").html(detail_msg.resiko_pkpt);
                $("#_msg_ket_pkpt").html(detail_msg.ket_pkpt);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_ms_pkpt ){
            var data_main = new FormData();
            data_main.append('id_ms_pkpt', id_ms_pkpt );

            $.ajax({
                url: "<?= $main_controller."delete_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_ms_pkpt ) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_ms_pkpt );
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."master/pkpt");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//
        function check_data(id_ms_pkpt, sts){
            var data_main = new FormData();
            data_main.append('id_ms_pkpt', id_ms_pkpt);
            data_main.append('param', sts);
            console.log(sts)
            $.ajax({
                url: "<?= $main_controller."check_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }
    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//

    
</script>