<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Registercustom extends CI_Model{

    function tabel_self($tb_name, $field){
        $field_all = '*,';
        $field_all .= "(select s2.".$field[2]." 
                        from $tb_name s2 
                        where s2.".$field[0]." = $tb_name.".$field[1]." 
                        ) 
                        AS parent";
        $this->db->select($field_all);
        $this->db->from($tb_name);
        $this->db->where("id_distrik_pem !=", "0");
        $this->db->where("is_del_pem", "0");
        $query=$this->db->get();

        return $query;
    }
    
    function cek_data($tb_name,$field, $kode){
        $field_all = '';
        for ($i=0; $i < count($field); $i++) { 
           if ($i == count($field) - 1) {
               $field_all .= $field[$i];
           }else{
                $field_all .= $field[$i].",";
           }
        }
        $this->db->select($field_all);
        $this->db->from($tb_name);
        for ($i=0; $i < count($field); $i++) {
            $this->db->or_where($field[$i], $kode[$i]);
        }
        $query=$this->db->get();

        return $query;
    }

    function cek_data_up($tb_name,$field, $kode){
        $field_all = '';
        for ($i=0; $i < count($field); $i++) { 
           if ($i == count($field) - 1) {
               $field_all .= $field[$i];
           }else{
                $field_all .= $field[$i].",";
           }
        }
        $this->db->select($field_all);
        $this->db->from($tb_name);
        for ($i=0; $i < count($field); $i++) {
            $this->db->or_where($field[$i], $kode[$i]);
            $this->db->where($field[0]." !=", $kode[0]);
        }
        $query=$this->db->get();

        return $query;
    }

}
?>