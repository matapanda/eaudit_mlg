<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_kejadian extends CI_Model{
    
    public function get_all_laporan(){
    	$data = $this->db->query("SELECT ad.id_admin as id_admin, ad.nama_admin as nama_admin,
                                    fc.id_feedback_category as id_feedback_category, fc.nama_feedback_category as nama_feedback_category,
                                    id_lap_kejadian, tgl_kejadian, no_tiket, nama_pelapor, lokasi_kejadian, tindak_lanjut_kejadian, foto_kejadian
                                FROM lap_kejadian lk
                                LEFT JOIN feedback_category fc ON lk.id_feedback_category = fc.id_feedback_category
                                LEFT JOIN admin ad ON lk.id_admin = ad.id_admin");
    	return $data->result();
    }

    public function get_all_laporan_where_id($id_admin = "0"){
    	$data = $this->db->query("SELECT ad.id_admin as id_admin, ad.nama_admin as nama_admin,
                                    fc.id_feedback_category as id_feedback_category, fc.nama_feedback_category as nama_feedback_category,
                                    id_lap_kejadian, tgl_kejadian, no_tiket, nama_pelapor, lokasi_kejadian, tindak_lanjut_kejadian, foto_kejadian
                                FROM lap_kejadian lk
                                LEFT JOIN feedback_category fc ON lk.id_feedback_category = fc.id_feedback_category
                                LEFT JOIN admin ad ON lk.id_admin = ad.id_admin WHERE lk.id_admin = '$id_admin'");
    	return $data->result();
    }

    public function get_all_laporan_where($where){
        $this->db->select("ad.id_admin as id_admin, ad.nama_admin as nama_admin,
                            fc.id_feedback_category as id_feedback_category, fc.nama_feedback_category as nama_feedback_category,
                            id_lap_kejadian, tgl_kejadian, no_tiket, nama_pelapor, lokasi_kejadian, tindak_lanjut_kejadian, foto_kejadian");
        $this->db->join('admin ad', 'lk.id_admin = ad.id_admin', 'left');
        $this->db->join('feedback_category fc', 'lk.id_feedback_category = fc.id_feedback_category', 'left');
        $data = $this->db->get_where("lap_kejadian lk", $where);

        return $data->row_array();
    }

}
?>