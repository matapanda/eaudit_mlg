<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikandatacustom extends CI_Model{
    public $db_custom;

    public function __construct(){
        parent::__construct(); 
        
        $this->db_custom = $this->load->database('data_pendidikan', TRUE);
    }
    
    
    public function get_data_excel_vs_seris(){
        $data = $this->db_custom->query("SELECT *, @id_fl_pd_bos:=id_fl_pd_bos, (SELECT COUNT(*) as c_data FROM data_bos WHERE id_fl_pd_bos = @id_fl_pd_bos) as c_data FROM `fl_pd_bos`");
        return $data->result();
    }

}
?>