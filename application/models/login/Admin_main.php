<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_main extends CI_Model{

    public function __construct(){
        parent::__construct(); 
    }

	public function select_admin($username, $password){
        $data = $this->db->query("
        SELECT id_admin,
            id_tipe_admin,
            jn_admin,
            id_dinas,
            email,
            username,
            nama_admin,
            nip_admin
        FROM admin WHERE (email = ".$username." or username=".$username.") 
            AND password='".$password."' 
            AND status_active= \"1\" 
            AND is_delete = \"0\"");

            // print_r($this->db->last_query());
        return $data->row_array();
	}

    public function select_admin_all($where){
        $this->db->select("id_admin, username, email, status_active, nama");
        $data = $this->db->get_where($this->tb_main, $where)->result();
        return $data;
    }

    public function insert_page_main($id_kategori, $nama_page, $next_page, $waktu, $id_admin){
        $data = $this->db->query("select insert_page_main('".$id_kategori."','".$nama_page."','".$next_page."','".$waktu."','".$id_admin."') as id_page;");
        return $data;
    }

    public function get_menu_main($where){
        $this->db->join("home_page_kategori b", "a.id_kategori = b.id_kategori");
        $data = $this->db->get_where("home_page_main a", $where)->result();
        return $data;
    }

    public function get_menu_kategori($where){
        $this->db->select("sha2(id_kategori, '256') as id_kategori, nama_kategori");
        $data = $this->db->get_where("home_page_kategori", $where)->result();
        return $data;
    }

    

}
?>