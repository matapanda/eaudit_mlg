<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taomaster extends CI_Controller {
    public $title = "TAO";
    public $tbl_main = "ms_tao_audit";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "tao";
        $data["title"] = $this->title;
		$data["list_data"] = $this->db->query("SELECT a.*, b.nama_ms_jn_audit AS nama_jenis, c.nama_ms_tj_audit AS nama_tujuan, d.nama_ms_ssrn_audit AS nama_sasaran FROM ms_tao_audit a LEFT JOIN ms_jn_audit b ON	a.ms_jn_audit_id = b.id_ms_jn_audit LEFT JOIN ms_tj_audit c ON a.ms_tj_audit_id = c.id_ms_tj_audit LEFT JOIN ms_ssrn_audit d ON a.ms_ssrn_audit_id = d.id_ms_ssrn_audit WHERE a.is_del_ms_tao_audit = '0'")->result();
        $data["jenis"] = $this->mm->get_data_all_where("ms_jn_audit", array("is_del_ms_jn_audit"=>"0","sts_ms_jn_audit"=>"1"));
        $data["tujuan"] = $this->mm->get_data_all_where("ms_tj_audit", array("is_del_ms_tj_audit"=>"0","sts_ms_tj_audit"=>"1"));
        $data["sasaran"] = $this->mm->get_data_all_where("ms_ssrn_audit", array("is_del_ms_ssrn_audit"=>"0","sts_ms_ssrn_audit"=>"1"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_ms_tao_audit"])){
        	$id_ms_tao_audit = $this->input->post('id_ms_tao_audit');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_ms_tao_audit"=>$id_ms_tao_audit));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'kd_tao_audit',
                'label'=>'kd_tao_audit',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'ket_tao_audit',
                'label'=>'ket_tao_audit',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'ms_jn_audit_id',
                'label'=>'ms_jn_audit_id',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'ms_tj_audit_id',
                'label'=>'ms_tj_audit_id',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'ms_ssrn_audit_id',
                'label'=>'ms_ssrn_audit_id',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_tao_audit"=>"",
                    "ket_tao_audit"=>"",
                    "ms_jn_audit_id"=>"",
                    "ms_tj_audit_id"=>"",
                    "ms_ssrn_audit_id"=>"",
                );

        if($this->val_form_insert()){
            
            $kd_tao_audit 	    = $this->input->post("kd_tao_audit", true);
            $ket_tao_audit 		= $this->input->post("ket_tao_audit", true);
            $ms_jn_audit_id 	= $this->input->post("ms_jn_audit_id", true);
            $ms_tj_audit_id 	= $this->input->post("ms_tj_audit_id", true);
            $ms_ssrn_audit_id 	= $this->input->post("ms_ssrn_audit_id", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  =     [[$type_pattern, $kd_tao_audit],
                                [$type_pattern, $ket_tao_audit],
                                [$type_pattern, $ms_jn_audit_id],
                                [$type_pattern, $ms_tj_audit_id],
                                [$type_pattern, $ms_ssrn_audit_id],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data($this->tbl_main, ["kd_tao_audit"], [$kd_tao_audit]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Kode Sudah Terdaftar pada Sistem");
                }else{
                    $data = [
                        "id_ms_tao_audit"=>"",
                        "kd_tao_audit"=>$kd_tao_audit,
                        "ket_tao_audit"=>$ket_tao_audit,
                        "ms_jn_audit_id"=>$ms_jn_audit_id,
                        "ms_tj_audit_id"=>$ms_tj_audit_id,
                        "ms_ssrn_audit_id"=>$ms_ssrn_audit_id,
                        "sts_ac_ms_tao_audit"=>"1"
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_tao_audit"]         = strip_tags(form_error('kd_tao_audit'));
            $msg_detail["ket_tao_audit"] 	    = strip_tags(form_error('ket_tao_audit'));
            $msg_detail["ms_jn_audit_id"] 	    = strip_tags(form_error('ms_jn_audit_id'));
            $msg_detail["ms_tj_audit_id"] 	    = strip_tags(form_error('ms_tj_audit_id'));
            $msg_detail["ms_ssrn_audit_id"] 	= strip_tags(form_error('ms_ssrn_audit_id'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_tao_audit"=>"",
                    "ket_tao_audit"=>"",
                    "ms_jn_audit_id"=>"",
                    "ms_tj_audit_id"=>"",
                    "ms_ssrn_audit_id"=>"",
                );

        if($this->val_form_insert()){
            $id_ms_tao_audit		= $this->input->post("id_ms_tao_audit", true);
            $kd_tao_audit 	        = $this->input->post("kd_tao_audit", true);
            $ket_tao_audit 			= $this->input->post("ket_tao_audit", true);
            $ms_jn_audit_id 		= $this->input->post("ms_jn_audit_id", true);
            $ms_tj_audit_id     	= $this->input->post("ms_tj_audit_id", true);
            $ms_ssrn_audit_id 	    = $this->input->post("ms_ssrn_audit_id", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $kd_tao_audit],
                                [$type_pattern, $ket_tao_audit],
                                [$type_pattern, $ms_jn_audit_id],
                                [$type_pattern, $ms_tj_audit_id],
                                [$type_pattern, $ms_ssrn_audit_id],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data_up($this->tbl_main, ["id_ms_tao_audit"," kd_tao_audit"], [$id_ms_tao_audit, $kd_tao_audit]);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Kode Sudah Terdaftar pada Sistem");
                }else{
                    $set = [
                        "kd_tao_audit"=>$kd_tao_audit,
                        "ket_tao_audit"=>$ket_tao_audit,
                        "ms_jn_audit_id"=>$ms_jn_audit_id,
                        "ms_tj_audit_id"=>$ms_tj_audit_id,
                        "ms_ssrn_audit_id"=>$ms_ssrn_audit_id,
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $where = array("id_ms_tao_audit"=>$id_ms_tao_audit);
                    $update = $this->mm->update_data($this->tbl_main, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_tao_audit"]         = strip_tags(form_error('kd_tao_audit'));
            $msg_detail["ket_tao_audit"] 		= strip_tags(form_error('ket_tao_audit'));
            $msg_detail["ms_jn_audit_id"] 	    = strip_tags(form_error('ms_jn_audit_id'));
            $msg_detail["ms_tj_audit_id"] 	    = strip_tags(form_error('ms_tj_audit_id'));
            $msg_detail["ms_ssrn_audit_id"] 	= strip_tags(form_error('ms_ssrn_audit_id'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

public function delete_data(){
    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
    $msg_detail = array(
                "id_ms_tao_audit"=>"",
            );

    if($_POST["id_ms_tao_audit"]){
        $id_ms_tao_audit = $this->input->post("id_ms_tao_audit");
        $where = array("id_ms_tao_audit"=>$id_ms_tao_audit);

        $set = array("is_del_ms_tao_audit"=>"1");

        // $delete_admin = $this->mm->delete_data($this->tbl_main, $where);
        $delete_admin = $this->mm->update_data($this->tbl_main, $set, $where);
        
        if($delete_admin){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
    }else{
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail["id_ms_tao_audit"]= strip_tags(form_error('id_ms_tao_audit'));        
    }

    $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_tao_audit"=>"0"));
    $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
    print_r(json_encode($res_msg));
}
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================
    public function check_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_ms_tao_audit"=>"",
                );

        if($_POST["id_ms_tao_audit"]){
            $id_ms_tao_audit = $this->input->post("id_ms_tao_audit");
            $param = $this->input->post("param");

            $where 	= array("id_ms_tao_audit"=>$id_ms_tao_audit);
            if ($param == "non_active") {
                $set 	= array("sts_ac_ms_tao_audit"=>"0");
            }else{
                $set 	= array("sts_ac_ms_tao_audit"=>"1");
            }
            
            
            $update_data = $this->mm->update_data($this->tbl_main, $set, $where);
            if($update_data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_ms_tao_audit"]= strip_tags(form_error('id_ms_tao_audit'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_ms_tao_audit"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
}
