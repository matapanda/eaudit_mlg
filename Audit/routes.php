<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


#-----------------------------autentication---------------------------
	$route['login'] 		= 'login/loginv0';
	$route['logout'] 		= 'login/logout';
#-----------------------------autentication---------------------------

#-----------------------------landingpage----------------------------------
	$route['admin/data_admin'] 		= 'admin/adminmain';
	$route['admin/user'] 		= 'admin/usermain';
#-----------------------------landing page----------------------------------





#-------------------------------------------------------------------------
#================================User=====================================
#-------------------------------------------------------------------------
	$route['user/register']	= 'user/register';
	$route['user/login']	= 'login/loginuser';

	$route['user/logout']	= 'login/logout/logout_user';

	$route['master/beranda']	= 'master/homeadmin';

	$route['master/distrik']		= 'master/distrikmaster';
	$route['master/skpd']			= 'master/skpdmaster';
	$route['master/golongan']		= 'master/golonganmaster';
	$route['master/jabatan']		= 'master/jabatanmaster';
	$route['master/jenis']			= 'master/jenismaster';
	$route['master/sasaran']		= 'master/sasaranmaster';
	$route['master/tujuan']			= 'master/tujuanmaster';
<<<<<<< HEAD
	$route['master/pkpt']			= 'master/pkptmaster';
	$route['master/tao']			= 'master/taomaster';
=======
	$route['master/aturan']			= 'master/aturanmaster';
	$route['master/pedoman']			= 'master/pedomanmaster';
>>>>>>> 70f9a53527eec0125d42dff07f250a75fd4f3e9f
#-------------------------------------------------------------------------
#================================User=====================================
#-------------------------------------------------------------------------